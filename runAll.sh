mvn clean
rm -rf allure-results
rm -rf allure-report
mvn test
allure generate target/allure-results
allure open allure-report

