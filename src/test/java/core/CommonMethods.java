package core;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class CommonMethods {

    protected SelenideElement findElementByAttribute(String attribute, String value) {
        return $(By.cssSelector(String.format("[" + attribute + "='%s']", value)));
    }
}
