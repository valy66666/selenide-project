package core;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.WebElement;
import poms.EngagementPage;
import poms.LoginPage;

import java.time.Duration;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$x;
import static com.codeborne.selenide.Selenide.executeJavaScript;

public class BaseTest {

    protected JDBC database = new JDBC();
    protected LoginPage loginPage = new LoginPage();
    protected EngagementPage engagementPage = new EngagementPage();

    public BaseTest() {
    }

    protected void sendKeys(SelenideElement el, String text) {
        el.shouldBe(visible, Duration.ofSeconds(30)).sendKeys(text);

    }

    protected SelenideElement findElementByText(String text) {
        return $x("//*[text()='" + text + "']");
    }

    protected void jsClick(SelenideElement el) {
        executeJavaScript("arguments[0].click();", el);

    }
}
