package core;


import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

public class ReadConfig {
    static Properties properties;

    public ReadConfig() {
        File src = new File("src/test/resources/config.properties");
        try {
            FileInputStream fileInputStream = new FileInputStream(src);
            properties = new Properties();
            properties.load(fileInputStream);
        } catch (Exception e) {
            System.out.println("Exception is: " + e.getMessage());
        }

    }

    protected static String getConfigValue(String key) {
        return properties.getProperty(key);

    }
}
