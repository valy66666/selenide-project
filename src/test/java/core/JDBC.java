package core;


import java.sql.*;
import java.util.ArrayList;


public class JDBC extends ReadConfig {


    public ArrayList<String> getEngagementsTitlesWithoutFilters() {
        ArrayList<String> engagementTitlesList = new ArrayList<>();
        String query1 = "Select top 10 * from Engagements";
        if (getConfigValue("environment").equals("dev")) {
            try (Connection conn = DriverManager.getConnection("jdbc:sqlserver://" + getConfigValue("dbDevHost") + ";database=" + getConfigValue("dbDevName") + ";encrypt=false", getConfigValue("dbDevUser"), getConfigValue("dbDevPass"));
                 Statement stmt = conn.createStatement();
            ) {
                ResultSet rs = stmt.executeQuery(query1);
                while (rs.next()) {
                    engagementTitlesList.add(rs.getString("Title"));
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else {
            System.err.println("THE SELECTED ENV DOESN'T EXIST!!!");
        }
        return engagementTitlesList;

    }

}


