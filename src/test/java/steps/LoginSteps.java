package steps;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import core.BaseTest;
import core.JDBC;
import io.cucumber.java.en.*;
import io.cucumber.java.eo.Se;
import org.junit.Assert;
import poms.LoginPage;

import java.sql.SQLException;
import java.time.Duration;
import java.util.List;

import static com.codeborne.selenide.Condition.visible;

public class LoginSteps extends BaseTest {


    @Given("user is on login page")
    public void user_is_on_login_page() {
        loginPage.emailField.shouldBe(visible, Duration.ofSeconds(30));
    }

    @When("user pass authentication")
    public void userPassAuthentication() {

    }

    @Then("user is redirected to engagement dashboard page")
    public void userIsRedirectedToEngagementDashboardPage() {
        findElementByText("Engagements").shouldBe(visible, Duration.ofSeconds(30));

    }
}
