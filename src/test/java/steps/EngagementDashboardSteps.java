package steps;

import com.codeborne.selenide.Selenide;
import core.BaseTest;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.Keys;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

import static com.codeborne.selenide.Condition.clickable;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.actions;
import static com.codeborne.selenide.Selenide.sleep;

public class EngagementDashboardSteps extends BaseTest {
    @Then("all columns are displayed on the engagement page")
    public void allColumnsAreDisplayedOnTheEngagementPage() {
        findElementByText("Engagement").shouldBe(visible, Duration.ofSeconds(10));
        findElementByText("Client").shouldBe(visible, Duration.ofSeconds(10));
        findElementByText("Status").shouldBe(visible, Duration.ofSeconds(10));
        findElementByText("Team Members").shouldBe(visible, Duration.ofSeconds(10));
        findElementByText("Active Work Orders").shouldBe(visible, Duration.ofSeconds(10));
        findElementByText("Team Feedback").shouldBe(visible, Duration.ofSeconds(10));
        findElementByText("Service Feedback").shouldBe(visible, Duration.ofSeconds(10));
        findElementByText("Audits").shouldBe(visible, Duration.ofSeconds(10));
        Selenide.sleep(5000);
    }

    @And("user clicks on status dropdown")
    public void userClickOnStatusDropdown() {
        engagementPage.statusDropdown.shouldBe(visible, Duration.ofSeconds(10)).click();
    }

    @And("disable all statuses in the filter")
    public void disableAllStatusesInTheFilters() {
        engagementPage.disableAllStatusFilters();
        actions().sendKeys(Keys.ESCAPE).build().perform();
    }

    @Then("all engagement titles are displayed without status filter applied")
    public void allEngagementTitlesAreDisplayedWithoutStatusFilterApplied() {
        int i = -1;
        for (String title : database.getEngagementsTitlesWithoutFilters()) {
            i++;
            Assert.assertEquals(title, engagementPage.getEngagementTitlesUI().get(i));

        }

    }

    @When("user enable filter by active status")
    public void userEnableFilterByActiveStatus() {
        engagementPage.applyActiveStatusFilter();
    }

    @Then("all displayed engagements have active status")
    public void allDisplayedEngagementsHaveActiveStatus() {
        for (String status : engagementPage.getEngagementStatusesUI()) {
            Assert.assertEquals("Active", status);
        }
    }

    @When("user enable filter by not started status")
    public void userEnableFilterByNotStartedStatus() {
        engagementPage.applyNotStartedStatusFilter();
    }

    @Then("all displayed engagements have not started status")
    public void allDisplayedEngagementsHaveNotStartedStatus() {
        for (String status : engagementPage.getEngagementStatusesUI()) {
            Assert.assertEquals("Not started", status);
        }
    }

    @When("user enable filter by completed status")
    public void userEnableFilterByCompletedStatus() {
        engagementPage.applyCompletedStatusFilter();
    }

    @Then("all displayed engagements have completed status")
    public void allDisplayedEngagementsHaveCompletedStatus() {
        for (String status : engagementPage.getEngagementStatusesUI()) {
            Assert.assertEquals("Completed", status);
        }
    }
}
