package steps;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.WebDriverRunner;
import core.BaseTest;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import poms.LoginPage;

import static com.codeborne.selenide.Selenide.closeWebDriver;


public class Hooks {
    protected static final Logger logger = LogManager.getLogger(Hooks.class);
    private final static String URL = "https://icy-plant-065a45a03.4.azurestaticapps.net/";

    @Before
    public void initialize() throws Exception {
        Configuration.browser = "chrome";
        Selenide.open(URL);
        WebDriverRunner.getWebDriver().manage().window().maximize();
        LoginPage obj = new LoginPage();
        obj.passAuthentication();

    }

    @After
    public void quit(Scenario scenario) {
        if (scenario.isFailed()) {
            TakesScreenshot ts = (TakesScreenshot) WebDriverRunner.getWebDriver();
            byte[] src = ts.getScreenshotAs(OutputType.BYTES);
            scenario.attach(src, "image/png", "screenshot");
        }
        closeWebDriver();
    }

}
