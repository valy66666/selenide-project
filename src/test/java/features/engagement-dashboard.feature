Feature:  Engagement Dashboard Feature

  @regression
  Scenario: Validate all columns are displayed on the Engagement Dashboard page
    Given user is redirected to engagement dashboard page
    Then all columns are displayed on the engagement page

  @regression
  Scenario: Validate engagement titles are displayed correct without filters applied
    Given user is redirected to engagement dashboard page
    And user clicks on status dropdown
    When disable all statuses in the filter
    Then all engagement titles are displayed without status filter applied

  @regression
  Scenario: Validate filter by Active status is working fine
    Given user is redirected to engagement dashboard page
    And user clicks on status dropdown
    When user enable filter by active status
    Then all displayed engagements have active status

  @regression
  Scenario: Validate filter by NotStarted status is working fine
    Given user is redirected to engagement dashboard page
    And user clicks on status dropdown
    When user enable filter by not started status
    Then all displayed engagements have not started status

  @regression
  Scenario: Validate filter by Completed status is working fine
    Given user is redirected to engagement dashboard page
    And user clicks on status dropdown
    When user enable filter by completed status
    Then all displayed engagements have completed status