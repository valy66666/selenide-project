package poms;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import java.time.Duration;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$x;

public class LoginPage {

    public final SelenideElement loginButton = $x("//button[text()='Log in as Amdaris team member']");
    public final SelenideElement emailField = $(By.name("email"));
    public final SelenideElement usernameTextField = $(By.name("loginfmt"));
    public final SelenideElement passwordTextField = $(By.name("passwd"));
    public final SelenideElement submitButton = $x("//input[@type='submit']");

    public void passAuthentication() {
        loginButton.shouldBe(visible, Duration.ofSeconds(30)).click();
        usernameTextField.shouldBe(visible, Duration.ofSeconds(30)).sendKeys("testuser.clientportal@amdaris.com");
        submitButton.shouldBe(visible, Duration.ofSeconds(30)).click();
        passwordTextField.shouldBe(visible, Duration.ofSeconds(30)).sendKeys("Clientportal1!");
        submitButton.shouldBe(visible, Duration.ofSeconds(30)).click();
        submitButton.shouldBe(visible, Duration.ofSeconds(30)).click();

    }


}
