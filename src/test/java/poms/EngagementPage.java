package poms;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import core.CommonMethods;
import io.cucumber.java.bs.A;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.*;

public class EngagementPage extends CommonMethods {

    public SelenideElement statusDropdown = $$(By.xpath("//div[@aria-haspopup='listbox']")).get(0);

    public void disableAllStatusFilters() {
        ArrayList<SelenideElement> statuses = new ArrayList<>();
        statuses.add(findElementByAttribute("data-value", "Active"));
        statuses.add(findElementByAttribute("data-value", "NotStarted"));
        statuses.add(findElementByAttribute("data-value", "Complete"));

        for (SelenideElement el : statuses) {
            if (el.getAttribute("aria-selected").equals("true")) {
                el.click();
            }
        }
    }

    public void applyActiveStatusFilter(){
        disableAllStatusFilters();
        findElementByAttribute("data-value", "Active").click();
    }

    public void applyNotStartedStatusFilter(){
        disableAllStatusFilters();
        findElementByAttribute("data-value", "NotStarted").click();
    }

    public void applyCompletedStatusFilter(){
        disableAllStatusFilters();
        findElementByAttribute("data-value", "Complete").click();
    }


    public List<String> getEngagementTitlesUI() {

        ElementsCollection elements = $$x("//a[contains(@href, 'work-orders') and contains(@class, 'MuiBox-root')]/div[1]/div[1]/div");
        elements.get(0).shouldBe(visible, Duration.ofSeconds(30));
        ArrayList<String> list = new ArrayList<>();
        for (SelenideElement el : elements) {
            el.scrollIntoView(true);
            list.add(el.getText());
        }
        return list;
    }

    public List<String> getEngagementStatusesUI() {

        ElementsCollection elements = $$x("//a[contains(@href, 'work-orders') and contains(@class, 'MuiBox-root')]/div[3]/div[1]/div/div/span/div/p");
        elements.get(0).shouldBe(visible, Duration.ofSeconds(30));
        ArrayList<String> list = new ArrayList<>();
        for (SelenideElement el : elements) {
            el.scrollIntoView(true);
            list.add(el.getText());
        }
        return list;
    }
}
