
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;


import static io.cucumber.junit.CucumberOptions.SnippetType.CAMELCASE;

@RunWith(Cucumber.class)
@CucumberOptions(

        plugin = {"pretty", "io.qameta.allure.cucumber7jvm.AllureCucumber7Jvm"},
        features = {
                "src/test/java/features"
        }
        , glue = {"steps"}
        , snippets = CAMELCASE
        , monochrome = true
//        , tags = "@regression"


)


public class TestRunner {
    @BeforeClass
    public static void initialize() throws Exception {

    }

    @AfterClass
    public static void quit() {
    }

}